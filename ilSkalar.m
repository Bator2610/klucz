function y = ilSkalar(v,w)

if length(v) ~= length(w)
    y = 'Blad: rozne dlugosci wektorow!'
else
    y = v * w';
end